/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.ui.school

import androidx.activity.compose.setContent
import androidx.compose.ui.Modifier
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.compose.ui.test.onNodeWithTag
import androidx.compose.ui.test.onNodeWithText
import androidx.compose.ui.test.performClick
import androidx.compose.ui.test.performTextClearance
import androidx.compose.ui.test.performTextInput
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.nycschools.data.di.fakeSchools
import com.example.nycschools.ui.MainActivity
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.RuleChain
import org.junit.runner.RunWith

/**
 * UI tests for [SchoolListScreen].
 */
@RunWith(AndroidJUnit4::class)
@HiltAndroidTest
class SchoolScreenTest {

    private var hiltRule = HiltAndroidRule(this)
    private val composeTestRule = createAndroidComposeRule<MainActivity>()

    @get:Rule
    val rule: RuleChain = RuleChain
        .outerRule(hiltRule)
        .around(composeTestRule)

    @Before
    fun setup() {
        hiltRule.inject()
        composeTestRule.activity.setContent {
            SchoolListScreen(modifier = Modifier, onClickSchool = {})
        }
    }

    @Test
    fun first_school_exists() {
        composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools.first().name.subSequence(0, 20).toString(),
            useUnmergedTree = true).assertExists()
    }

    @Test
    fun can_filter_down_to_second_school() {
        val firstSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[0].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)
        val secondSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[1].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)
        val thirdSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[2].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)

        firstSchool.assertExists()
        secondSchool.assertExists()
        thirdSchool.assertExists()

        val searchBox = composeTestRule.onNodeWithTag("searchBox")
        searchBox.performTextInput(fakeSchools[1].name.subSequence(0, 5).toString())

        composeTestRule.onNodeWithText("Search").assertExists().performClick()

        firstSchool.assertDoesNotExist()
        secondSchool.assertExists()
        thirdSchool.assertDoesNotExist()
    }

    @Test
    fun all_schools_are_shown_when_search_is_cleared() {
        val firstSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[0].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)
        val secondSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[1].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)
        val thirdSchool = composeTestRule.onNodeWithText(
            substring = true,
            text = fakeSchools[2].name.subSequence(0, 20).toString(),
            useUnmergedTree = true)

        firstSchool.assertExists()
        secondSchool.assertExists()
        thirdSchool.assertExists()

        val searchBox = composeTestRule.onNodeWithTag("searchBox")
        searchBox.performTextInput(fakeSchools[1].name.subSequence(0, 5).toString())

        composeTestRule.onNodeWithText("Search").assertExists().performClick()

        firstSchool.assertDoesNotExist()
        secondSchool.assertExists()
        thirdSchool.assertDoesNotExist()

        searchBox.performTextClearance()

        firstSchool.assertExists()
        secondSchool.assertExists()
        thirdSchool.assertExists()
    }
}
