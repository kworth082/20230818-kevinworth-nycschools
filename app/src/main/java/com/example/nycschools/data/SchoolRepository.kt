/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.data

import SchoolApiModel
import android.util.Log
import com.example.nycschools.data.domain.SatScore
import com.example.nycschools.data.domain.School
import com.example.nycschools.data.local.database.SatScoreEntity
import com.example.nycschools.data.local.database.SchoolDao
import com.example.nycschools.data.local.database.SchoolEntity
import com.example.nycschools.data.network.SatScoreApiModel
import com.example.nycschools.data.network.SchoolDataSource
import com.example.nycschools.data.network.SchoolRemoteDataSource
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.filterNotNull
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

interface SchoolRepository {
    val schools: Flow<List<School>>

    fun getSchool(dbn: String): Flow<School>
    suspend fun refreshAllSchools()
    suspend fun search(name: String): Flow<List<School>>
    suspend fun add(school: SchoolApiModel)

    suspend fun getSatScore(schoolDbn: String): Flow<SatScore>
}

class DefaultSchoolRepository @Inject constructor(
    private val schoolDao: SchoolDao,
    private val schoolRemoteDataSource: SchoolDataSource,
) : SchoolRepository {

    override val schools: Flow<List<School>> =
        schoolDao.getSchools().map { items -> items.map { it.toSchool() } }

    override fun getSchool(dbn: String): Flow<School> {
        return flow {
//            schoolDao.deleteSchoolByDbn(dbn)
            val school = schoolDao.getSchool(dbn).firstOrNull()
            var isLoading = false
            if (school == null) {
                isLoading = true
                CoroutineScope(Dispatchers.IO).launch {
//                    delay(2000)
                    refreshSchool(dbn)
                    isLoading = false
                }
            }
            schoolDao.getSchool(dbn).collect { schoolEntity ->
                when {
                    schoolEntity != null -> emit(schoolEntity.toSchool())
                    else -> {
                        when (isLoading) {
                            true -> Log.w("SchoolRepository", "Unrecognized dbn $dbn while refreshing schools")
                            false -> throw IllegalArgumentException("Unrecognized dbn $dbn. All schools refreshed.")
                        }
                    }
                }
            }
        }
    }

    override suspend fun refreshAllSchools() {
        // TODO: With more time, add error handling such as no network connectivity, etc. Currently
        // the exceptions are caught at view level where their message is shown to the (confused) user
        val response = schoolRemoteDataSource.getAllSchools()
        if (response.isSuccessful) {
            response.body()?.let {
                addAll(it)
            }
        } // TODO: In addition to things like catching exceptions, also need an `else` here
    }

    private suspend fun refreshSchool(dbn: String) {
        // TODO: With more time, add error handling such as no network connectivity, etc. Currently
        // the exceptions are caught at view level where their message is shown to the (confused) user
        val response = schoolRemoteDataSource.getSchool(dbn)
        if (response.isSuccessful) {
            response.body()?.firstOrNull()?.let {
                add(it)
            }
        } // TODO: In addition to things like catching exceptions, also need an `else` here
    }

    override suspend fun add(school: SchoolApiModel) {
        schoolDao.upsertSchool(school.toEntity())
    }

    override suspend fun search(name: String): Flow<List<School>> =
        schoolDao.searchSchools(name).map { schools ->
            schools.map { entity ->
                entity.toSchool()
            }
        }

    private suspend fun addAll(schools: List<SchoolApiModel>) {
        // TODO: Given more time we would want to wrap this in a `AppDatabase.withTransaction { ... }` block
        // so that all of the inserts will be executed within a single database transaction
        schools.forEach {
            schoolDao.upsertSchool(it.toEntity())
        }
    }

    override suspend fun getSatScore(schoolDbn: String): Flow<SatScore> =
        schoolDao.getSatScore(schoolDbn)
            .onStart {
                refreshSatScore(schoolDbn)
            }
            .filterNotNull()
            .map {
                it.toSatScore()
            }

    private suspend fun refreshSatScore(schoolDbn: String) {
        val response = schoolRemoteDataSource.getSatScore(schoolDbn)
        if (response.isSuccessful) {
            response.body()?.firstOrNull()?.let {
                schoolDao.upsertSatScore(it.toEntity())
            }
        }
    }
}

fun SchoolEntity.toSchool(): School {
    return School(
        dbn = dbn,
        name = name,
    )
}

fun SchoolApiModel.toEntity(): SchoolEntity {
    return SchoolEntity(
        dbn = dbn,
        name = name,
        overviewParagraph = overviewParagraph,
        academicOpportunities1 = academicOpportunities1,
        academicOpportunities2 = academicOpportunities2,
        neighborhood = neighborhood,
        phoneNumber = phoneNumber,
        faxNumber = faxNumber,
        schoolEmail = schoolEmail,
        website = website,
        grades2018 = grades2018,
        finalGrades = finalGrades,
        totalStudents = totalStudents,
        attendanceRate = attendanceRate,
        directions1 = directions1,
        primaryAddressLine1 = primaryAddressLine1,
        city = city,
        zip = zip,
        stateCode = stateCode,
        latitude = latitude,
        longitude = longitude,
        borough = borough,
        campusName = campusName,
        startTime = startTime,
        endTime = endTime,
        graduationRate = graduationRate,
        collegeCareerRate = collegeCareerRate,
    )
}

fun SatScoreEntity.toSatScore(): SatScore {
    return SatScore(
        schoolDbn = schoolDbn,
        readingAverage = readingAverage,
        mathAverage = mathAverage,
        writingAverage = writingAverage,
    )
}

fun SatScoreApiModel.toEntity(): SatScoreEntity {
    return SatScoreEntity(
        schoolDbn = schoolDbn,
        schoolName = schoolName,
        numTestTakers = numTestTakers,
        readingAverage = criticalReadingAverage,
        mathAverage = mathAverage,
        writingAverage = writingAverage,
    )
}