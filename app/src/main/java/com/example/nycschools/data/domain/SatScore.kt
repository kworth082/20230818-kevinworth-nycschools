package com.example.nycschools.data.domain

data class SatScore(
    val schoolDbn: String, // "01M292"
    val readingAverage: String? = null, // "355" OR "s"
    val mathAverage: String? = null, // "404" OR "s"
    val writingAverage: String? = null, // "363" OR "s"
)
