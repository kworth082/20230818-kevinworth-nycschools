package com.example.nycschools.data.domain

data class School(
    val dbn: String,
    val name: String,
    val satScore: SatScore? = null,
)
