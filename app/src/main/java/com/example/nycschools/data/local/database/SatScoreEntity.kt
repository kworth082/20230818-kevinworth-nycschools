package com.example.nycschools.data.local.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "SatScore")
data class SatScoreEntity(
    @PrimaryKey
    val schoolDbn: String, // "01M292"
    val schoolName: String, // "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"
    val numTestTakers : String? = null, // "29" OR "s"
    val readingAverage: String? = null, // "355" OR "s"
    val mathAverage: String? = null, // "404" OR "s"
    val writingAverage: String? = null, // "363" OR "s"
)