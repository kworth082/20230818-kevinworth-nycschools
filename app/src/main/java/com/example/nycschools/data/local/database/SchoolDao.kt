package com.example.nycschools.data.local.database

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface SchoolDao {
    // Schools
    @Query("Select * FROM School WHERE dbn = :dbn")
    fun getSchool(dbn: String): Flow<SchoolEntity?>

    @Query("SELECT * FROM School ORDER BY name ASC")
    fun getSchools(): Flow<List<SchoolEntity>>

    @Query("SELECT * FROM School WHERE name LIKE '%' || :name || '%' ORDER BY name ASC")
    fun searchSchools(name: String): Flow<List<SchoolEntity>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertSchool(item: SchoolEntity)

    @Delete
    suspend fun deleteSchool(item: SchoolEntity)

    @Query("DELETE FROM School WHERE dbn = :dbn")
    suspend fun deleteSchoolByDbn(dbn: String)

    // SAT Scores
    @Query("Select * FROM SatScore WHERE schoolDbn = :schoolDbn")
    fun getSatScore(schoolDbn: String): Flow<SatScoreEntity?>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun upsertSatScore(score: SatScoreEntity)
}