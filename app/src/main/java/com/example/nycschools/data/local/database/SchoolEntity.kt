/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.data.local.database

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "School")
data class SchoolEntity(
    @PrimaryKey
    var dbn: String,  // "02M260"
    val name: String,  // "Clinton School Writers & Artists, M.S. 260"
    val overviewParagraph: String? = null,  // "Students who are prepared for college must have an education..."
    val academicOpportunities1: String? = null,  // "Free college courses at neighboring universities"
    val academicOpportunities2: String? = null,  // "International Travel, Special Arts Programs, Music, Internships..."
    val neighborhood: String? = null,  // "Chelsea-Union Sq"
    val phoneNumber: String? = null,  // "212-524-4360"
    val faxNumber: String? = null,  // "212-524-4365"
    val schoolEmail: String? = null,  // "admissions@theclintonschool.net"
    val website: String? = null,  // "www.theclintonschool.net"
    val grades2018: String? = null,  // "6-11"
    val finalGrades: String? = null,  // "6-12"
    val totalStudents: Int? = null,  // 376
    val attendanceRate: Double? = null,  // 0.970000029
    val directions1: String? = null,  // "See theclintonschool.net for more information."
    val primaryAddressLine1: String? = null,  // "10 East 15th Street"
    val city: String? = null,  // "Manhattan"
    val zip: String? = null,  // "10003"
    val stateCode: String? = null,  // "NY"
    val latitude: String? = null,  // "40.73653"
    val longitude: String? = null,  // "-73.9927"
    val borough: String? = null,  // "MANHATTAN"
    val campusName: String? = null,  // "Prospect Heights Educational Campus"
    val startTime: String? = null,  // "8:20am"
    val endTime: String? = null,  // "2:45pm"
    val graduationRate: Double? = null,  // 0.612999976
    val collegeCareerRate: Double? = null,  // 0.486000001
)
