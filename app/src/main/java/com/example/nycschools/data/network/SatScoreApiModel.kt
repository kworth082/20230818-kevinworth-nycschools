package com.example.nycschools.data.network

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SatScoreApiModel(
    @SerialName("dbn")
    val schoolDbn: String, // "01M292"

    @SerialName("school_name")
    val schoolName: String, // "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES"

    // Looking at the data, the following 4 properties are either all numbers or the string "s"
    @SerialName("num_of_sat_test_takers")
    val numTestTakers : String? = null, // "29"

    @SerialName("sat_critical_reading_avg_score")
    val criticalReadingAverage: String? = null, // "355"

    @SerialName("sat_math_avg_score")
    val mathAverage: String? = null, // "404"

    @SerialName("sat_writing_avg_score")
    val writingAverage: String?, // "363"
)
