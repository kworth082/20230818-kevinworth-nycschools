import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class SchoolApiModel(
    @SerialName("dbn")
    val dbn: String,  // "02M260"

    @SerialName("school_name")
    val name: String,  // "Clinton School Writers & Artists, M.S. 260"

    @SerialName("boro")
    val boro: String? = null,  // "M"

    @SerialName("overview_paragraph")
    val overviewParagraph: String? = null,  // "Students who are prepared for college must have an education..."

    @SerialName("school_10th_seats")
    val school10thSeats: Int? = null,  // 1

    @SerialName("academicopportunities1")
    val academicOpportunities1: String? = null,  // "Free college courses at neighboring universities"

    @SerialName("academicopportunities2")
    val academicOpportunities2: String? = null,  // "International Travel, Special Arts Programs, Music, Internships..."

    @SerialName("ell_programs")
    val ellPrograms: String? = null,  // "English as a New Language"

    @SerialName("neighborhood")
    val neighborhood: String? = null,  // "Chelsea-Union Sq"

    @SerialName("building_code")
    val buildingCode: String? = null,  // "M868"

    @SerialName("location")
    val location: String? = null,  // "10 East 15th Street, Manhattan NY 10003 (40.736526, -73.992727)"

    @SerialName("phone_number")
    val phoneNumber: String? = null,  // "212-524-4360"

    @SerialName("fax_number")
    val faxNumber: String? = null,  // "212-524-4365"

    @SerialName("school_email")
    val schoolEmail: String? = null,  // "admissions@theclintonschool.net"

    @SerialName("website")
    val website: String? = null,  // "www.theclintonschool.net"

    @SerialName("subway")
    val subway: String? = null,  // "1, 2, 3, F, M to 14th St - 6th Ave; 4, 5, L, Q to 14th St-Union Square; 6, N, R to 23rd St"

    @SerialName("bus")
    val bus: String? = null,  // "BM1, BM2, BM3, BM4, BxM10, BxM6, BxM7, BxM8, BxM9, M1, M101, M102, M103, M14A, M14D, M15, M15-SBS, M2, M20, M23, M3, M5, M7, M8, QM21, X1, X10, X10B, X12, X14, X17, X2, X27, X28, X37, X38, X42, X5, X63, X64, X68, X7, X9"

    @SerialName("grades2018")
    val grades2018: String? = null,  // "6-11"

    @SerialName("finalgrades")
    val finalGrades: String? = null,  // "6-12"

    @SerialName("total_students")
    val totalStudents: Int? = null,  // 376

    @SerialName("extracurricular_activities")
    val extracurricularActivities: String? = null,  // "CUNY College Now, Technology, Model UN, Student Government..."

    @SerialName("school_sports")
    val schoolSports: String? = null,  // "Cross Country, Track and Field, Soccer, Flag Football, Basketball"

    @SerialName("attendance_rate")
    val attendanceRate: Double? = null,  // 0.970000029

    @SerialName("pct_stu_enough_variety")
    val pctStuEnoughVariety: Double? = null,  // 0.899999976

    @SerialName("pct_stu_safe")
    val pctStuSafe: Double? = null,  // 0.970000029

    @SerialName("school_accessibility_description")
    val schoolAccessibilityDescription: Int? = null,  // "1"

    @SerialName("directions1")
    val directions1: String? = null,  // "See theclintonschool.net for more information."

    @SerialName("requirement1_1")
    val requirement1_1: String? = null,  // "Course Grades: English (87-100), Math (83-100), Social Studies (90-100), Science (88-100)"

    @SerialName("requirement2_1")
    val requirement2_1: String? = null,  // "Standardized Test Scores: English Language Arts (2.8-4.5), Math (2.8-4.5)"

    @SerialName("requirement3_1")
    val requirement3_1: String? = null,  // "Attendance and Punctuality"

    @SerialName("requirement4_1")
    val requirement4_1: String? = null,  // "Writing Exercise"

    @SerialName("requirement5_1")
    val requirement5_1: String? = null,  // "Group Interview (On-Site)"

    @SerialName("offer_rate1")
    val offerRate1: String? = null,  // "—57% of offers went to this group"

    @SerialName("program1")
    val program1: String? = null,  // "M.S. 260 Clinton School Writers & Artists"

    @SerialName("code1")
    val code1: String? = null,  // "M64A"

    @SerialName("interest1")
    val interest1: String? = null,  // "Humanities & Interdisciplinary"

    @SerialName("method1")
    val method1: String? = null,  // "Screened"

    @SerialName("seats9ge1")
    val seats9ge1: String? = null,  // 80

    @SerialName("grade9gefilledflag1")
    val grade9gefilledflag1: String? = null,  // "Y"

    @SerialName("grade9geapplicants1")
    val grade9geapplicants1: String? = null,  // 1515

    @SerialName("seats9swd1")
    val seats9swd1: String? = null,  // 16

    @SerialName("grade9swdfilledflag1")
    val grade9swdfilledflag1: String? = null,  // "Y"

    @SerialName("grade9swdapplicants1")
    val grade9swdapplicants1: String? = null,  // 138

    @SerialName("seats101")
    val seats101: String? = null,  // "Yes—9"

    @SerialName("admissionspriority11")
    val admissionspriority11: String? = null,  // "Priority to continuing 8th graders"

    @SerialName("admissionspriority21")
    val admissionspriority21: String? = null,  // "Then to Manhattan students or residents"

    @SerialName("admissionspriority31")
    val admissionspriority31: String? = null,  // "Then to New York City residents"

    @SerialName("grade9geapplicantsperseat1")
    val grade9geapplicantsperseat1: String? = null,  // "19"

    @SerialName("grade9swdapplicantsperseat1")
    val grade9swdapplicantsperseat1: String? = null,  // "9"

    @SerialName("primary_address_line_1")
    val primaryAddressLine1: String? = null,  // "10 East 15th Street"

    @SerialName("city")
    val city: String? = null,  // "Manhattan"

    @SerialName("zip")
    val zip: String? = null,  // "10003"

    @SerialName("state_code")
    val stateCode: String? = null,  // "NY"

    @SerialName("latitude")
    val latitude: String? = null,  // "40.73653"

    @SerialName("longitude")
    val longitude: String? = null,  // "-73.9927"

    @SerialName("community_board")
    val communityBoard: String? = null,  // "5"

    @SerialName("council_district")
    val councilDistrict: String? = null,  // "2"

    @SerialName("census_tract")
    val censusTract: String? = null,  // "52"

    @SerialName("bin")
    val bin: String? = null,  // "1089902"

    @SerialName("bbl")
    val bbl: String? = null,  // "1008420034"

    @SerialName("nta")
    val nta: String? = null,  // "Hudson Yards-Chelsea-Flatiron-Union Square"

    @SerialName("borough")
    val borough: String? = null,  // "MANHATTAN"

    @SerialName("campus_name")
    val campusName: String? = null,  // "Prospect Heights Educational Campus"

    @SerialName("transfer")
    val transfer: Int? = null,  // 1

    @SerialName("eligibility1")
    val eligibility1: String? = null,  // ...

    @SerialName("addtl_info1")
    val additionalInfo1: String? = null,  // ...

    @SerialName("start_time")
    val startTime: String? = null,  // "8:20am"

    @SerialName("end_time")
    val endTime: String? = null,  // "2:45pm"

    @SerialName("prgdesc1")
    val programDescription1: String? = null,  // "Provides arts instruction to all students and integrates the arts into the core curriculum."

    @SerialName("advancedplacement_courses")
    val advancedPlacementCourses: String? = null,  // "AP English, AP Environmental Science, AP US History"

    @SerialName("shared_space")
    val sharedSpace: String? = null,  // "Yes"

    @SerialName("psal_sports_boys")
    val psalSportsBoys: String? = null,  // "Baseball, Basketball, Cross Country, Fencing"

    @SerialName("psal_sports_girls")
    val psalSportsGirls: String? = null,  // "Basketball, Cross Country, Indoor Track, Outdoor Track, Softball, Volleyball"

    @SerialName("psal_sports_coed")
    val psalSportsCoed: String? = null,  // "Stunt"

    @SerialName("graduation_rate")
    val graduationRate: Double? = null,  // 0.612999976

    @SerialName("college_career_rate")
    val collegeCareerRate: Double? = null,  // 0.486000001
)
