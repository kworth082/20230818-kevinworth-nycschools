package com.example.nycschools.data.network

import SchoolApiModel
import retrofit2.Response
import javax.inject.Inject

interface SchoolDataSource {
    suspend fun getAllSchools(): Response<List<SchoolApiModel>>
    suspend fun getSchool(dbn: String): Response<List<SchoolApiModel>>
    suspend fun getSatScore(schoolDbn: String): Response<List<SatScoreApiModel>>
}

const val QUERY_PARAM_KEY_DBN = "dbn"

class SchoolRemoteDataSource @Inject constructor(
    private val schoolService: SchoolService,
) : SchoolDataSource {
    override suspend fun getAllSchools(): Response<List<SchoolApiModel>> {
        return schoolService.getSchools()
    }

    override suspend fun getSchool(dbn: String): Response<List<SchoolApiModel>> {
        val queryParams = hashMapOf(Pair(QUERY_PARAM_KEY_DBN, dbn))
        return schoolService.getSchools(queryParams)
    }

    override suspend fun getSatScore(schoolDbn: String): Response<List<SatScoreApiModel>> {
        val queryParams = hashMapOf(Pair(QUERY_PARAM_KEY_DBN, schoolDbn))
        return schoolService.getSatScores(queryParams)
    }
}