package com.example.nycschools.data.network

import SchoolApiModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.QueryMap

interface SchoolService {
    @GET("resource/s3k6-pzi2.json")
    suspend fun getSchools(
        @QueryMap options: Map<String, String>? = HashMap(),
    ): Response<List<SchoolApiModel>>

    @GET("resource/f9bf-2cp4.json")
    suspend fun getSatScores(
        @QueryMap options: Map<String, String>? = HashMap(),
    ): Response<List<SatScoreApiModel>>
}