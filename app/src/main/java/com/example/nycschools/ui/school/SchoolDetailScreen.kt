package com.example.nycschools.ui.school

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentHeight
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.nycschools.R
import com.example.nycschools.data.domain.SatScore
import com.example.nycschools.data.domain.School
import com.example.nycschools.ui.theme.NycSchoolsTheme

@Composable
fun SchoolDetailScreen(
    viewModel: SchoolDetailViewModel = hiltViewModel()
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    SchoolDetailScreen(uiState)
}

@Composable
fun SchoolDetailScreen(
    uiState: SchoolDetailUiState
) {
    Column(
        Modifier
            .verticalScroll(rememberScrollState())
    ) {
        when (uiState) {
            SchoolDetailUiState.Loading -> {
                Text("Loading...", Modifier.fillMaxWidth(), style = MaterialTheme.typography.headlineMedium, textAlign = TextAlign.Center)
            }
            is SchoolDetailUiState.Success -> {
                NameHeader(uiState.data.name)
                uiState.data.satScore?.let {
                    SatScoreSection(it)
                }
            }
            is SchoolDetailUiState.Error -> {
                Text("Something went wrong", Modifier.fillMaxWidth(), style = MaterialTheme.typography.headlineSmall, textAlign = TextAlign.Center)
                uiState.throwable.message?.let {
                    Spacer(modifier = Modifier.height(8.dp))
                    Divider(thickness = 1.dp)
                    Spacer(modifier = Modifier.height(8.dp))
                    Text(it, Modifier.fillMaxWidth(), style = MaterialTheme.typography.titleSmall)
                }
            }
        }

    }
}

@Composable
fun NameHeader(name: String) {
    Text(
        name,
        modifier = Modifier
            .background(MaterialTheme.colorScheme.primaryContainer)
            .fillMaxWidth()
            .padding(8.dp),
        style = MaterialTheme.typography.headlineMedium)
}

@Composable
fun SatScoreSection(satScore: SatScore) {
    Column() {
        Text(
            text = stringResource(id = R.string.sat_section_title),
            modifier = Modifier
                .padding(horizontal = 8.dp)
                .fillMaxWidth(),
            style = MaterialTheme.typography.titleLarge,
            textAlign = TextAlign.Center,
        )
        Row(Modifier.fillMaxWidth()) {
            Score(Modifier.weight(1f), stringResource(id = R.string.sat_reading_title), satScore.readingAverage)
            Score(Modifier.weight(1f), stringResource(id = R.string.sat_math_title), satScore.mathAverage)
            Score(Modifier.weight(1f), stringResource(id = R.string.sat_writing_title), satScore.writingAverage)
        }
    }
}

@Composable
fun Score(
    modifier: Modifier = Modifier,
    title: String,
    score: String?
) {
    Column(modifier.wrapContentHeight()) {
        Text(title, modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
        Text(score ?: "N/A", modifier = Modifier.fillMaxWidth(), textAlign = TextAlign.Center)
    }
}

@Preview
@Composable
private fun PreviewSchoolDetailScreen() {
    val uiState = SchoolDetailUiState.Success(
         data = School("a", "Liberation Diploma Plus High School")
    )

    NycSchoolsTheme {
        Surface {
            SchoolDetailScreen(uiState)
        }
    }
}

@Preview
@Composable
private fun PreviewSchoolDetailScreenWithSat() {
    val uiState = SchoolDetailUiState.Success(
        data = School("a", "Liberation Diploma Plus High School", satScore = SatScore("01M292", "355", "404", "363"))
    )

    NycSchoolsTheme {
        Surface {
            SchoolDetailScreen(uiState)
        }
    }
}

@Preview(showBackground = true, showSystemUi = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
private fun PreviewSchoolDetailScreenLoading() {
    val uiState = SchoolDetailUiState.Loading

    NycSchoolsTheme {
        Surface {
            SchoolDetailScreen(uiState)
        }
    }
}

@Preview(showBackground = true, showSystemUi = true)
@Composable
private fun PreviewSchoolDetailScreenError() {
    val uiState = SchoolDetailUiState.Error(IllegalArgumentException("Invalid database id"))

    NycSchoolsTheme {
        Surface {
            SchoolDetailScreen(uiState)
        }
    }
}
