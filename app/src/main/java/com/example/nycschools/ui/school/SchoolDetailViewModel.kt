/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.ui.school

import android.util.Log
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nycschools.data.SchoolRepository
import com.example.nycschools.data.domain.School
import com.example.nycschools.ui.ARGUMENT_DBN
import com.example.nycschools.ui.school.SchoolDetailUiState.Error
import com.example.nycschools.ui.school.SchoolDetailUiState.Loading
import com.example.nycschools.ui.school.SchoolDetailUiState.Success
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SchoolDetailViewModel @Inject constructor(
    private val savedStateHandle: SavedStateHandle,
    private val schoolRepository: SchoolRepository,
) : ViewModel() {

    // In a real-world project, throwing this exception here would be insufficient as it would
    // simply cause the app to crash and leave the user confused and frustrated. Instead we
    // would log an error to the server and display a helpful error message to the user
    private val schoolDbn: String = savedStateHandle.get<String>(ARGUMENT_DBN)
        ?: throw IllegalStateException("Unable to show details without dbn passed in")

    private val _uiState: MutableStateFlow<SchoolDetailUiState> = MutableStateFlow(Loading)
    val uiState: StateFlow<SchoolDetailUiState> = _uiState
        .onEach { state ->
            // Launch in separate co-routine so that fetching missing SAT score doesn't
            // hold up other data we already have
            CoroutineScope(Dispatchers.IO).launch {
                getSatScoreIfMissing(state)
            }
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), Loading)

    init {
        viewModelScope.launch {
            schoolRepository.getSchool(schoolDbn)
                .distinctUntilChanged()
                .catch {
                    Log.e("SchoolDetailViewModel", "Error while collecting school with dbn: $schoolDbn", it)
                    if (_uiState.value is Loading) {
                        _uiState.emit(Error(it))
                    }
                }.collect {
                    _uiState.emit(Success(it))
                }
        }
    }

    private suspend fun getSatScoreIfMissing(state: SchoolDetailUiState) {
        // Only check school if UiState is Success...
        (state as? Success)?.data?.let { school ->
            if (school.satScore == null) {
                // TODO: add error handling here so user isn't confused why SAT scores are simply blank
                schoolRepository.getSatScore(school.dbn).firstOrNull()?.let { score ->
                    _uiState.update { Success(school.copy(satScore = score)) }
                }
            }
        }
    }
}

sealed interface SchoolDetailUiState {
    object Loading : SchoolDetailUiState
    data class Error(val throwable: Throwable) : SchoolDetailUiState
    data class Success(val data: School) : SchoolDetailUiState
}
