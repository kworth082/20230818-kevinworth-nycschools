/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.ui.school

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material3.Button
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.compose.collectAsStateWithLifecycle
import com.example.nycschools.R
import com.example.nycschools.data.di.fakeSchools
import com.example.nycschools.data.domain.School
import com.example.nycschools.ui.theme.NycSchoolsTheme

@Composable
fun SchoolListScreen(
    modifier: Modifier = Modifier,
    viewModel: SchoolListViewModel = hiltViewModel(),
    onClickSchool: (schoolDbn: String) -> Unit,
) {
    val uiState by viewModel.uiState.collectAsStateWithLifecycle()
    when (uiState) {
        SchoolListUiState.Loading ->
            // Given more time, let's make this a more elaborate composable that looks good
            Text("Loading Schools")
        is SchoolListUiState.Success ->
            SchoolListScreen(
                schools = (uiState as SchoolListUiState.Success).data,
                onSearch = viewModel::searchSchool,
                onClearSearch = { viewModel.getAllSchools() },
                modifier = modifier,
                onClickSchool = onClickSchool,
            )
        is SchoolListUiState.Error ->
            // Given more time, let's make this a more elaborate composable that looks good
            Text("An error has occurred: \n\n${(uiState as SchoolListUiState.Error).throwable.message}")
    }
}

@Composable
internal fun SchoolListScreen(
    schools: List<School>,
    onSearch: (name: String) -> Unit,
    onClearSearch: () -> Unit,
    modifier: Modifier = Modifier,
    onClickSchool: (schoolDbn: String) -> Unit,
) {
    Column(modifier) {
        var searchQuery by remember { mutableStateOf("") }
        Row(
            modifier = Modifier
                .fillMaxWidth()
                .padding(bottom = 24.dp),
            horizontalArrangement = Arrangement.spacedBy(16.dp)
        ) {
            TextField(
                modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f)
                    .testTag("searchBox"),
                value = searchQuery,
                onValueChange = {
                    searchQuery = it
                    if (it.isEmpty()) {
                        onClearSearch()
                    }
                },
                placeholder = { Text("Filter by school name") }
            )

            Button(
                modifier = Modifier.width(96.dp),
                onClick = { onSearch(searchQuery) },
                enabled = searchQuery.isNotEmpty()
            ) {
                Text(stringResource(id = R.string.search_button_title))
            }
        }

        LazyColumn {
            itemsIndexed(schools) { index, school ->
                Divider(thickness = 1.dp)
                SchoolSummary(school = school, onClick = onClickSchool)
                if (index == schools.lastIndex) {
                    Divider(thickness = 1.dp)
                }
            }
        }
    }
}

@Composable
fun SchoolSummary(
    school: School,
    onClick: (schoolDbn: String) -> Unit,
) {
    Row(
        modifier = Modifier
            .height(48.dp)
            .clickable { onClick(school.dbn) },
        verticalAlignment = Alignment.CenterVertically,

    ) {
        Text(
            text = "${school.name}",
            style = MaterialTheme.typography.titleMedium,
            maxLines = 1,
            overflow = TextOverflow.Ellipsis
        )
    }
}

// Previews

@Preview(showBackground = true)
@Composable
private fun DefaultPreview() {
    NycSchoolsTheme {
        SchoolListScreen(
            fakeSchools,
            onSearch = {},
            onClearSearch = {},
            onClickSchool = {},
        )
    }
}

@Preview(showBackground = true, widthDp = 350)
@Composable
private fun PortraitPreview() {
    NycSchoolsTheme {
        SchoolListScreen(
            fakeSchools,
            onSearch = {},
            onClearSearch = {},
            onClickSchool = {},
        )
    }
}
