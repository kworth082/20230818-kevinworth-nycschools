/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.ui.school

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch
import com.example.nycschools.data.SchoolRepository
import com.example.nycschools.data.domain.School
import com.example.nycschools.ui.school.SchoolListUiState.Error
import com.example.nycschools.ui.school.SchoolListUiState.Loading
import com.example.nycschools.ui.school.SchoolListUiState.Success
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.firstOrNull
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class SchoolListViewModel @Inject constructor(
    private val schoolRepository: SchoolRepository
) : ViewModel() {

    private val _schools: MutableStateFlow<List<School>> = MutableStateFlow(emptyList())

    val uiState: StateFlow<SchoolListUiState> = _schools // schoolRepository.schools
        .onStart {
            if (_schools.value.isEmpty()) {
                schoolRepository.refreshAllSchools()
                getAllSchools()
            }
        }
        .map<List<School>, SchoolListUiState>(SchoolListUiState::Success)
        .catch {
            Log.e("SchoolViewModel", "Error while collecting schools", it)
            emit(Error(it))
        }
        .stateIn(viewModelScope, SharingStarted.WhileSubscribed(5000), Loading)

    fun getAllSchools() {
        Log.w("SchoolListViewModel", "getAllSchools")
        viewModelScope.launch {
            schoolRepository.schools.collect { allSchools ->
                _schools.update { allSchools }
            }
        }
    }

    fun searchSchool(name: String) {
        viewModelScope.launch {
            schoolRepository.search(name).collect { results ->
                _schools.update { results }
            }
        }
    }
}

sealed interface SchoolListUiState {
    object Loading : SchoolListUiState
    data class Error(val throwable: Throwable) : SchoolListUiState
    data class Success(val data: List<School>) : SchoolListUiState
}
