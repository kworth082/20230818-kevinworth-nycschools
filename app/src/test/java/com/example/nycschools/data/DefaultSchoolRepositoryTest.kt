/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.data

import com.example.nycschools.data.di.FakeSchoolRemoteDataSource
import com.example.nycschools.data.local.database.SatScoreEntity
import com.example.nycschools.data.local.database.SchoolDao
import com.example.nycschools.data.local.database.SchoolEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Unit tests for [DefaultSchoolRepository].
 */
@OptIn(ExperimentalCoroutinesApi::class) // TODO: Remove when stable
class DefaultSchoolRepositoryTest {

    @Test
    fun schools_newItemSaved_itemIsReturned() = runTest {
        val repository = DefaultSchoolRepository(FakeSchoolDao(), FakeSchoolRemoteDataSource())

        repository.search("Repository")

        assertEquals(repository.schools.first().size, 1)
    }

}

// TODO: Add/implement all the missing methods to get this to compile
private class FakeSchoolDao : SchoolDao {

    private val data = mutableListOf<SchoolEntity>()
    override fun getSchool(dbn: String): Flow<SchoolEntity?> {
        TODO("Not yet implemented FakeSchoolDao.getSchool()")
    }

    override fun getSchools(): Flow<List<SchoolEntity>> = flow {
        emit(data)
    }

    override fun searchSchools(name: String): Flow<List<SchoolEntity>> {
        TODO("Not yet implemented FakeSchoolDao.searchSchools")
    }

    override suspend fun upsertSchool(item: SchoolEntity) {
        data.add(0, item)
    }

    override suspend fun deleteSchool(item: SchoolEntity) {
        TODO("Not yet implemented FakeSchoolDao.deleteSchool")
    }

    override suspend fun deleteSchoolByDbn(dbn: String) {
        TODO("Not yet implemented FakeSchoolDao.deleteSchoolByDbn")
    }

    override fun getSatScore(schoolDbn: String): Flow<SatScoreEntity?> {
        TODO("Not yet implemented FakeSchoolDao.getSatScore")
    }

    override suspend fun upsertSatScore(score: SatScoreEntity) {
        TODO("Not yet implemented FakeSchoolDao.upsertSatScore")
    }
}
