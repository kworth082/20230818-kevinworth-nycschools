/*
 * Copyright (C) 2022 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.nycschools.ui.school


import SchoolApiModel
import com.example.nycschools.data.SchoolRepository
import com.example.nycschools.data.domain.SatScore
import com.example.nycschools.data.domain.School
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Test

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@OptIn(ExperimentalCoroutinesApi::class) // TODO: Remove when stable
class SchoolListViewModelTest {
    @Test
    fun uiState_initiallyLoading() = runTest {
        val viewModel = SchoolListViewModel(FakeSchoolRepository())
        assertEquals(viewModel.uiState.first(), SchoolListUiState.Loading)
    }

    @Test
    fun uiState_onItemSaved_isDisplayed() = runTest {
        val viewModel = SchoolListViewModel(FakeSchoolRepository())
        assertEquals(viewModel.uiState.first(), SchoolListUiState.Loading)
    }
}

private class FakeSchoolRepository : SchoolRepository {

    private val data = mutableListOf<School>()

    override val schools: Flow<List<School>>
        get() = flow { emit(data.toList()) }

    override fun getSchool(dbn: String): Flow<School> {
        TODO("Not yet implemented")
    }

    override suspend fun refreshAllSchools() {
        TODO("Not yet implemented FakeSchoolRepository.refreshAllSchools()")
    }

    override suspend fun search(name: String): Flow<List<School>> {
        TODO("Not yet implemented FakeSchoolRepository.search()")
    }

    override suspend fun add(school: SchoolApiModel) {
        TODO("Not yet implemented FakeSchoolRepository.add()")
    }

    override suspend fun getSatScore(schoolDbn: String): Flow<SatScore> {
        TODO("Not yet implemented FakeSchoolRepository.getSatScore()")
    }
}
